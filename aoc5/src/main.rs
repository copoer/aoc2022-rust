use std::io;
use std::io::prelude::*;

fn main() {
    let stdin = io::stdin();
    let mut input_len = 0;
    let mut v: Vec<Vec<char>> = Vec::new();
    for line in stdin.lock().lines() {
        let line_str: String = match line {
            Ok(s) => s,
            Err(_) => break
        };
        if line_str.len() < 1 { break };
        input_len = line_str.len();
        while input_len/4 >= v.len() {
            v.push(Vec::new())
        }
        for c in 1..input_len-1 {
            if (c-1) % 4 == 0 {
                let i = (c-1)/4;
                if line_str.chars().nth(c).unwrap() != ' ' {
                    v[i].push(line_str.chars().nth(c).unwrap());
                }
            }
        }

    }
    for c in 1..input_len-1 {
        if (c-1) % 4 == 0 {
            let i = (c-1)/4;
            v[i].reverse();
        }
    }
    for line in stdin.lock().lines() {
        let line_str: String = match line {
            Ok(s) => s,
            Err(_) => break
        };
        if line_str.len() < 1 { break };
        let split: Vec<&str> = line_str.split(" ").collect();
        let num_to_move: u16 = split[1].parse().unwrap();
        let from: usize = split[3].parse().unwrap();
        let to: usize = split[5].parse().unwrap();
        for _ in 0..num_to_move {
            let mv = v[from-1].pop().unwrap();
            v[to-1].push(mv);
        }
    }
    for c in 1..input_len-1 {
        if (c-1) % 4 == 0 {
            let i = (c-1)/4;
            println!("{}", v[i].pop().unwrap())
        }
    }

}
