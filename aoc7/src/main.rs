use std::io;
use std::io::prelude::*;
use std::collections::HashMap;

/**
 * Building a partial tree to record directory tree of dir sizes
 * Needs to be bigger than 1523801
 */
#[derive(Clone)]
struct Node {
    index: u32,
    name: String,
    files: HashMap<String, u64>,
    children: Vec<Node>
}
impl Node {
    fn get(&self, i: u32) -> Option<&Node> {
        let mut res : Option<&Node> = None;
        if self.index == i {
            res = Some(self);
        } else {
            for child in self.children.iter() {
                res = child.get(i);
                match res {
                    Some(_) => break,
                    None => ()
                }
            }
        }
        res
    }
    fn get_mut(&mut self, i: u32) -> Option<&mut Node> {
        let mut res : Option<&mut Node> = None;
        if self.index == i {
            res = Some(self);
        } else {
            for child in self.children.iter_mut() {
                res = child.get_mut(i);
                match res {
                    Some(_) => break,
                    None => ()
                }
            }
        }
        res
    }
    fn get_size(&self) -> u64 {
        let mut size = self.files.values().sum();
        for child in self.children.iter() {
            size += child.get_size();
        }
        size
    }
    fn find_parent_index(&self, child_i: u32) -> Option<u32> {
        match self.children.iter().find(|e| e.index == child_i) {
            Some(_) => Some(self.index),
            None => {
                let mut res : Option<u32> = None; 
                for child in self.children.iter() {
                    res = child.find_parent_index(child_i);
                    match res {
                        Some(_) => break,
                        None => ()
                    }
                }
                res
            }
        }
    }
}

fn main() {
    let stdin = io::stdin();
    let mut total: u64 = 0;
    let mut head : Node = Node {
        index: 0,
        name: String::from("/"),
        files: HashMap::new(),
        children: Vec::new()
    };
    let mut curr_index: u32 = 0;
    let mut index_count: u32 = 1;
    for line in stdin.lock().lines() {
        let line_str: String = match line {
            Ok(s) => s,
            Err(_) => break
        };
        let len = line_str.len();
        if len < 1 { break };
        let split: Vec<&str> = line_str.split(" ").collect();
        match split[0] {
            "$" => match split[1] {
                "cd" => match split[2] {
                    ".." => {
                        // Find parent
                        curr_index = match head.find_parent_index(curr_index) {
                            Some(e) => e,
                            None => panic!("No parent found")
                        };
                    },
                    "/" => {
                        curr_index = 0;
                    },
                    _ => {
                        let name: String = split[2].parse().unwrap();
                        // Set curr_node
                        curr_index = match head.get(curr_index) {
                            Some(curr_node) => {
                                match curr_node.children.iter().find(|e| e.name == name) {
                                    Some(child) => child.index,
                                    None => panic!("Unable to find dir"),
                                }
                            },
                            None => panic!("Unable to get curr_index")
                        };
                    }
                },
                "ls" => (),
                _ => panic!("Invalid state"),
            },
            "dir" => {
                let name: String = split[1].parse().unwrap();
                let new_node = Node {
                    name: name.clone(),
                    index: index_count,
                    files: HashMap::new(),
                    children: Vec::new()
                };
                // No Duplicate Folders
                if head.get(curr_index).unwrap().children.iter().find(|e| e.name == name).is_none() {
                    head.get_mut(curr_index).unwrap().children.push(new_node.clone());
                    index_count += 1;
                }
            },
            _ => {
                // Increase size of curr_node
                head.get_mut(curr_index).unwrap().files.entry(split[1].parse::<String>().unwrap()).or_insert(split[0].parse::<u64>().unwrap());
            },
        };
    }

    for n in 0..index_count {
        let node = head.get(n).unwrap();
        println!("Index {}, Name {}, Size {}, Files {}, Dir {}", n, node.name, node.get_size(), node.files.len(), node.children.len());
    }

    for n in 0..index_count {
        let curr_total = head.get(n).unwrap().get_size();
        if curr_total <= 100000 {
            total += curr_total;
        }
    }
    println!("Part 1: {}", total);

    let free = 70000000-head.get_size();
    let needed = 30000000-free;
    let mut min = 70000000;
    for n in 0..index_count {
        let curr_total = head.get(n).unwrap().get_size();
        if curr_total >= needed && curr_total < min {
            min = curr_total;
        }
    }
    println!("Part 2: {}", min);
}
