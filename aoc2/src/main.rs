use std::io;
use std::io::prelude::*;

#[derive(Clone)]
struct Hand {
    letter: char,
    value: u16,
}

fn main() {
    let rock = Hand {
        letter: 'r',
        value: 1,
    };
    let paper = Hand {
        letter: 'p',
        value: 2,
    };
    let scissors = Hand {
        letter: 's',
        value: 3
    };
    let stdin = io::stdin();
    let mut total = 0;
    let mut total_two = 0;
    for line in stdin.lock().lines() {
        let line_str: String = match line {
            Ok(s) => s,
            Err(_) => {
                continue;
            }
        };
        if line_str.chars().count() < 3 { break };
        let first = line_str.chars().nth(0).expect("Missing first char");
        let last = line_str.chars().nth(2).expect("Missing last char");
        let first = match first {
            'A' => rock.clone(),
            'B' => paper.clone(),
            'C' => scissors.clone(),
            _ => continue
        };
        let last_o = match last {
            'X' => rock.clone(),
            'Y' => paper.clone(),
            'Z' => scissors.clone(),
            _ => continue
        };
        let result = match last_o.letter {
            y if y == first.letter => 3,
            y if y == 'r' && first.letter == 's' => 6,
            y if y == 's' && first.letter == 'p' => 6,
            y if y == 'p' && first.letter == 'r' => 6,
            _ => 0
        };
        total += last_o.value + result;
        
        total_two += match last {
            'X' => 0 + match first.letter {
                'r' => 3,
                's' => 2,
                'p' => 1,
                _ => panic!("Invalid State")
            },
            'Y' => 3 + match first.letter {
                'r' => 1,
                's' => 3,
                'p' => 2,
                _ => panic!("Invalid State")
            },
            'Z' => 6 + match first.letter {
                'r' => 2,
                's' => 1,
                'p' => 3,
                _ => panic!("Invalid State")
            },
            _ => panic!("Invalid state"),
        };
    };
    println!("Part 1: {}", total);
    println!("Part 2: {}", total_two);
}
