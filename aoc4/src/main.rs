use std::io;
use std::io::prelude::*;

fn main() {
    let stdin = io::stdin();
    let mut total: u32 = 0;
    for line in stdin.lock().lines() {
        let line_str: String = match line {
            Ok(s) => s,
            Err(_) => break
        };
        let len = line_str.len();
        if len < 6 { break };
        let split: Vec<&str> = line_str.split(",").collect();
        let first: Vec<&str> = split[0].split("-").collect();
        let second: Vec<&str> = split[1].split("-").collect();
        let first: (u32, u32) = (first[0].parse().unwrap(), first[1].parse().unwrap());
        let second: (u32, u32) = (second[0].parse().unwrap(), second[1].parse().unwrap());
        let result = match first {
            (x,y) if x >= second.0 && y <= second.1 => 1,
            (x,y) if x <= second.0 && y >= second.1 => 1,
            _ => 0
        };
        total += result;
    }
    println!("Total: {}", total);
}
