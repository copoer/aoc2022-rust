use std::io;
use std::io::prelude::*;

fn main() {
    let stdin = io::stdin();
    let mut total: u32 = 0;
    for line in stdin.lock().lines() {
        let line_str: String = match line {
            Ok(s) => s,
            Err(_) => break
        };
        let len = line_str.len();
        if len < 1 { break };
        let first = &line_str[0..len/2];
        let secound = &line_str[len/2..len];
        'full: for fl in first.chars() {
            for sl in secound.chars() {
                if fl == sl {
                    let lnum = match fl as u32 {
                        65..=90 => (fl as u32) - 38,
                        _ => (fl as u32) - 96
                    };
                    total += lnum;
                    break 'full;
                }
            }
        }
    };
    println!("Total: {}", total);
}
