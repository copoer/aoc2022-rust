use std::io;
use std::io::prelude::*;

fn main() {
    let stdin = io::stdin();
    let mut total: usize = 0;
    for line in stdin.lock().lines() {
        let line_str: String = match line {
            Ok(s) => s,
            Err(_) => break
        };
        let len = line_str.len();
        if len < 1 { break };
        'find: for i in 0..len-4 {
            let slice = &line_str[i..i+4];
            if 
                slice.matches(slice.chars().nth(0).unwrap()).count() < 2 && 
                slice.matches(slice.chars().nth(1).unwrap()).count() < 2 && 
                slice.matches(slice.chars().nth(2).unwrap()).count() < 2 && 
                slice.matches(slice.chars().nth(3).unwrap()).count() < 2 
            {
                total = i+4;
                break 'find;
            }
        }
    }
    println!("Total: {}", total);
}
