use std::io;
use std::io::prelude::*;

fn main() {
    let stdin = io::stdin();
    let mut max: u32 = 0;
    let mut acc: u32 = 0;
    let mut elves: Vec<u32> = Vec::new();
    for line in stdin.lock().lines() {
        let line_str: String = match line {
            Ok(s) => s,
            Err(_) => {
                elves.push(acc);
                acc = 0;
                continue;
            }
        };
        let line_num: u32 = match line_str.parse() {
            Ok(n) => n,
            Err(_) => {
                elves.push(acc);
                acc = 0;
                continue
            }
        };
        acc += line_num;
        if acc >= max {
            max = acc;
        }
    };
    println!("Part 1: {}", max);
    elves.sort();
    println!("Part 2: {}", elves.pop().unwrap()+elves.pop().unwrap()+elves.pop().unwrap());
}
